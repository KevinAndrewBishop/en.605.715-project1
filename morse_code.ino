/*
  Morse Code

  Converts a message received from Serial Monitor into morse code. The morse code message is 
  sent via a blinking LED.

Author: Kevin Bishop
Date: 2020-02-08
*/


int ledPin = 13;
String userString = "";
String morseString = "";
char character;
char sentinel = '*';
int dotMilliseconds = 100;
int dashMilliseconds = dotMilliseconds*3;
int charEndMilliseconds = dotMilliseconds*3;
int spaceMilliseconds = dotMilliseconds*7;


//Maps a string to dots and dashes
String morseEncode(String s){
  String encodedString = "";
  s.toLowerCase();
  for (int i = 0; i < s.length(); i++){
    encodedString.concat(morseEncodeChar(s.charAt(i)));
    encodedString.concat('|'); //marks the end of a character
  }
  return encodedString;
}

//Maps a single character to its morse equivalent
//Note: this section borrowed from https://www.geeksforgeeks.org/morse-code-implementation/
String morseEncodeChar(char x) { 
  
  // refer to the Morse table 
  // image attached in the article 
  switch (x) { 
  case 'a': 
    return ".-"; 
  case 'b': 
    return "-..."; 
  case 'c': 
    return "-.-."; 
  case 'd': 
    return "-.."; 
  case 'e': 
    return "."; 
  case 'f': 
    return "..-."; 
  case 'g': 
    return "--."; 
  case 'h': 
    return "...."; 
  case 'i': 
    return ".."; 
  case 'j': 
    return ".---"; 
  case 'k': 
    return "-.-"; 
  case 'l': 
    return ".-.."; 
  case 'm': 
    return "--"; 
  case 'n': 
    return "-."; 
  case 'o': 
    return "---"; 
  case 'p': 
    return ".--."; 
  case 'q': 
    return "--.-"; 
  case 'r': 
    return ".-."; 
  case 's': 
    return "..."; 
  case 't': 
    return "-"; 
  case 'u': 
    return "..-"; 
  case 'v': 
    return "...-"; 
  case 'w': 
    return ".--"; 
  case 'x': 
    return "-..-"; 
  case 'y': 
    return "-.--"; 
  // for space 
  case 'z': 
    return "--.."; 
  case ' ':
    return " ";
  } 
  return "";
}

//maps a morse string of dots and dashes to its corresponding LED blink
void morseToLED(String morse){
  for (int i = 0; i<morse.length(); i++){
    if (morse.charAt(i) == '.'){
//      Serial.println("dot");
      digitalWrite(ledPin, HIGH);
      delay(dotMilliseconds);
      digitalWrite(ledPin, LOW);
      delay(dotMilliseconds);
    }
    if (morse.charAt(i) == '-'){
//      Serial.println("dash");
      digitalWrite(ledPin, HIGH);
      delay(dashMilliseconds);
      digitalWrite(ledPin, LOW);
      delay(dotMilliseconds);
    }
    if (morse.charAt(i) == '|'){
//      Serial.println("charend");
      delay(charEndMilliseconds);
    }
    if (morse.charAt(i) == ' '){
//      Serial.println("space");
      delay(spaceMilliseconds);
    }
  }
}

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(9600); 
}

// the loop function runs over and over again forever
void loop() {
  //read input until you hit a sentinel character
  while(Serial.available()>0){
    character = Serial.read();
    if (character == sentinel){
      break;
    }
    userString.concat(character);
  }
  //encode the string and blink out the morse code
  if (character == sentinel){
      morseString = morseEncode(userString);
      
      Serial.print("User entered:");
      Serial.println(userString);
      Serial.print("Encoded string:");
      Serial.println(morseString);
      
      morseToLED(morseString);
      //now that it's been translated, reset strings to empty
      userString = "";
      morseString = "";
      character = '-';
  }
  
}